# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from bypass_ai_server.models.base_model_ import Model
from bypass_ai_server import util


class DataChunk(Model):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    def __init__(self, left_motor_speed: float=None, right_motor_speed: float=None, confidence: float=None, is_completed: bool=None):  # noqa: E501
        """DataChunk - a model defined in Swagger

        :param left_motor_speed: The left_motor_speed of this DataChunk.  # noqa: E501
        :type left_motor_speed: float
        :param right_motor_speed: The right_motor_speed of this DataChunk.  # noqa: E501
        :type right_motor_speed: float
        :param confidence: The confidence of this DataChunk.  # noqa: E501
        :type confidence: float
        :param is_completed: The is_completed of this DataChunk.  # noqa: E501
        :type is_completed: bool
        """
        self.swagger_types = {
            'left_motor_speed': float,
            'right_motor_speed': float,
            'confidence': float,
            'is_completed': bool
        }

        self.attribute_map = {
            'left_motor_speed': 'left_motor_speed',
            'right_motor_speed': 'right_motor_speed',
            'confidence': 'confidence',
            'is_completed': 'is_completed'
        }
        self._left_motor_speed = left_motor_speed
        self._right_motor_speed = right_motor_speed
        self._confidence = confidence
        self._is_completed = is_completed

    @classmethod
    def from_dict(cls, dikt) -> 'DataChunk':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The DataChunk of this DataChunk.  # noqa: E501
        :rtype: DataChunk
        """
        return util.deserialize_model(dikt, cls)

    @property
    def left_motor_speed(self) -> float:
        """Gets the left_motor_speed of this DataChunk.


        :return: The left_motor_speed of this DataChunk.
        :rtype: float
        """
        return self._left_motor_speed

    @left_motor_speed.setter
    def left_motor_speed(self, left_motor_speed: float):
        """Sets the left_motor_speed of this DataChunk.


        :param left_motor_speed: The left_motor_speed of this DataChunk.
        :type left_motor_speed: float
        """

        self._left_motor_speed = left_motor_speed

    @property
    def right_motor_speed(self) -> float:
        """Gets the right_motor_speed of this DataChunk.


        :return: The right_motor_speed of this DataChunk.
        :rtype: float
        """
        return self._right_motor_speed

    @right_motor_speed.setter
    def right_motor_speed(self, right_motor_speed: float):
        """Sets the right_motor_speed of this DataChunk.


        :param right_motor_speed: The right_motor_speed of this DataChunk.
        :type right_motor_speed: float
        """

        self._right_motor_speed = right_motor_speed

    @property
    def confidence(self) -> float:
        """Gets the confidence of this DataChunk.


        :return: The confidence of this DataChunk.
        :rtype: float
        """
        return self._confidence

    @confidence.setter
    def confidence(self, confidence: float):
        """Sets the confidence of this DataChunk.


        :param confidence: The confidence of this DataChunk.
        :type confidence: float
        """

        self._confidence = confidence

    @property
    def is_completed(self) -> bool:
        """Gets the is_completed of this DataChunk.


        :return: The is_completed of this DataChunk.
        :rtype: bool
        """
        return self._is_completed

    @is_completed.setter
    def is_completed(self, is_completed: bool):
        """Sets the is_completed of this DataChunk.


        :param is_completed: The is_completed of this DataChunk.
        :type is_completed: bool
        """

        self._is_completed = is_completed
