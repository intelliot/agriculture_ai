import logging
import os

def setup_logging(log_file='logs/bypass_ai.log'):
    # Check for log file directory
    log_dir = os.path.dirname(log_file)
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)

    # Configure logging
    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s [%(levelname)s] - %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S',
        handlers=[
            logging.FileHandler(log_file),  # Streaming to log file
            logging.StreamHandler()  # Streaming to console
        ]
    )


def remove_external_stream_handler():
    root_logger = logging.getLogger()

    # Iterate over the root logger's handlers and remove the external StreamHandler
    for handler in root_logger.handlers[:]:
        if isinstance(handler, logging.StreamHandler) and handler.level == logging.INFO:
            root_logger.removeHandler(handler)