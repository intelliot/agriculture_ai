#!/usr/bin/env python3
import logging

from logger import setup_logging

import connexion


from bypass_ai_server import encoder
from configparser import ConfigParser
from bypass_ai_server.services.inference_service import load_model



def main():

    setup_logging()

    logging.info("Application is starting...")

    logging.info("Yolo model initiation...")

    load_model()

    app = connexion.App(__name__, specification_dir='./swagger/')
    app.app.json_encoder = encoder.JSONEncoder
    app.add_api('swagger_v2.yaml', arguments={'title': 'Obstacle Bypassing AI'}, pythonic_params=True)

    config = ConfigParser()
    config.read('config.ini')
    port = config.getint('flask', 'port')

    app.run(port=port)


if __name__ == '__main__':
    main()
