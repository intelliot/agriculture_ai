import configparser
import json
import logging
import random
import time

import connexion
import cv2
from flask import Response, jsonify, stream_with_context

from bypass_ai_server.models import RegisterAgentBody
from bypass_ai_server.models.compute_control_commands_response import ComputeControlCommandsResponse  # noqa: E501
from bypass_ai_server.models.data_chunk import DataChunk  # noqa: E501
from bypass_ai_server.models.get_stream_response import GetStreamResponse  # noqa: E501
from bypass_ai_server.services import agent_manager
from bypass_ai_server.services.inference_service import detect, load_model
from bypass_ai_server.services.rtp_service import get_rtp_handler, start_rtp_stream, APIError, ConfigError


def initiate_config():
    config = configparser.ConfigParser()
    config.read('./config.ini')
    # config.read('../config.ini')
    return config


def get_rtsp_handle():
    try:
        rtp_handle = get_rtp_handler()
        logging.info(f"RTP Handle : {rtp_handle}")
        success = start_rtp_stream()

        if not success:
            logging.error("Failed to start RTP stream.")
            raise APIError(f"Could not start the RTP stream")

        return rtp_handle
    except ConfigError as e:
        logging.error(f"Configuration error: {e}")
    except APIError as e:
        logging.error(f"API error: {e}")
        raise


def pre_process_data():
    config = initiate_config()

    try:
        confidence_threshold = config.get('CONTROL', 'confidence_threshold')
    except configparser.Error as e:
        raise ConfigError(f"Error reading configuration: {e}")

    # rtsp_url = "rtsp://100.66.26.169:8554/test"  # Replace with your RTSP stream URL
    rtsp_url = get_rtsp_handle()

    model = load_model()
    # Read the RTSP stream
    cap = cv2.VideoCapture(rtsp_url)

    if not cap.isOpened():
        logging.error("Failed to open RTSP stream.")
    return model, cap, confidence_threshold


def pre_process_dummy_data():
    config = initiate_config()

    try:
        confidence_threshold = float(config.get('CONTROL', 'confidence_threshold'))
    except configparser.Error as e:
        raise ConfigError(f"Error reading configuration: {e}")

    # rtsp_url = "rtsp://100.66.26.169:8554/test"  # Replace with your RTSP stream URL
    # rtsp_url = get_rtsp_handle()

    model = load_model()
    # Read the RTSP stream
    cap = cv2.VideoCapture('camera_feed.mp4')

    if not cap.isOpened():
        logging.error("Failed to open RTSP stream.")
    return model, cap, confidence_threshold


def process_data(model, cap, confidence_threshold):
    try:
        total_time = 0.0
        frame_count = 0

        while True:
            ret, frame = cap.read()

            if not ret:
                break
            start_time = time.time()

            processed_frame, data_chunk = detect(frame, model)

            end_time = time.time()
            detection_time = end_time - start_time

            total_time += detection_time
            frame_count += 1

            data_dict = data_chunk.to_dict()
            # print(data_dict)

            data_json = json.dumps(data_dict)

            # Notifying the agent when the confidence is lower than the threshold
            if float(data_chunk.confidence) <= confidence_threshold:
                data_chunk.is_completed = True
                agent_manager.notify_agent()
            yield data_json.encode()

            # Check if the stream should be ended
            if data_chunk.is_completed:
                break
        if frame_count > 0:
            average_time_per_frame = total_time / frame_count
            logging.info(f"Average time taken for control command calculation per frame: {average_time_per_frame:.4f} seconds")

        cap.release()
        cv2.destroyAllWindows()
    except ConfigError as e:
        logging.error(f"Configuration error: {e}")
        error_response = {"error": "Configuration error: {e}"}
        yield json.dumps(error_response).encode()  # Return a JSON error response with a 500 status code
    except APIError as e:
        logging.error(f"API error: {e}")
        error_response = {"error": "API error: {e}"}
        yield json.dumps(error_response).encode()  # Return a JSON error response with a 500 status code
    except Exception as e:
        logging.error(f"Unexpected error: {e.with_traceback()}")
        error_response = {"error": "An unexpected error occurred: {e}"}
        yield json.dumps(error_response).encode() # Return a JSON error response with a 500 status code
    finally:
        if 'cap' in locals() and cap.isOpened():
            cap.release()
        cv2.destroyAllWindows()


def generate_dummy_data():
    data_list = []
    load_model()
    for _ in range(10):
        left_motor_speed = random.uniform(0.0, 1.0)
        right_motor_speed = random.uniform(0.0, 1.0)
        confidence = random.uniform(0.0, 1.0)
        is_completed = False

        data_chunk = DataChunk(
            left_motor_speed=left_motor_speed,
            right_motor_speed=right_motor_speed,
            confidence=confidence,
            is_completed=is_completed
        )

        data_list.append(data_chunk)

    # Set is_completed=True for the last data chunk
    if data_list:
        data_list[-1].is_completed = True

    for data_chunk in data_list:
        data_dict = data_chunk.to_dict()
        data_json = json.dumps(data_dict)
        yield data_json.encode()
        time.sleep(1)


def compute_control_commands_get():  # noqa: E501
    """request AI to start computing control commands

    Request AI to compute controls # noqa: E501


    :rtype: ComputeControlCommandsResponse
    """

    model, cap, confidence_threshold = pre_process_data()
    data = process_data(model, cap, confidence_threshold)

    response = Response(stream_with_context(data), mimetype='application/json')

    return response


def data_stream_get():  # noqa: E501
    """Retrieve data stream

     # noqa: E501


    :rtype: List[DataChunk]
    """
    # print(notify_agent)
    # agent_manager.notify_agent()
    # data = generate_dummy_data()
    # response = Response(stream_with_context(data), mimetype='application/json',)

    model, cap, confidence_threshold = pre_process_dummy_data()
    data = process_data(model, cap, confidence_threshold)

    response = Response(stream_with_context(data), mimetype='application/json')
    return response


def g_et_control_stream_get():  # noqa: E501
    """Returns handler corresponding to the stream of control commands

    Calls for the control stream # noqa: E501


    :rtype: GetStreamResponse
    """
    return 'do some magic!'


def register_callback_url(body):  # noqa: E501
    """Register Callback URL of the Agent

     # noqa: E501

    :param body:
    :type body: dict | bytes

    :rtype: str
    """
    if connexion.request.is_json:
        body = RegisterAgentBody.from_dict(connexion.request.get_json())  # noqa: E501
        agent_manager.register_callback_url(body.callback_url)

    return Response('Agent registered successfully!')
