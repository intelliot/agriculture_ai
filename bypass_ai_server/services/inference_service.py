import configparser
import logging
import time

from bypass_ai_server.logger import setup_logging

import cv2
import torch as torch
import torchvision as torchvision
from PIL import Image, ImageDraw
import torch
import torchvision.transforms as transforms
import torchvision
# import matplotlib.pyplot as plt
import numpy as np
from bypass_ai_server.models.data_chunk import DataChunk
from bypass_ai_server.services.model_manager import ModelManager


class ConfigError(Exception):
    pass


def initiate_config():
    config = configparser.ConfigParser()
    config.read('./config.ini')
    # config.read('../config.ini')
    return config


def load_model():
    model = ModelManager.get_model()
    return model


# model = torch.hub.load('ultralytics/yolov5', 'custom', path='../weights/best.pt')
# model = torch.hub.load('ultralytics/yolov5', 'yolov5s')
# img = Image.open('./models/img.jpg')
# img = Image.open('../models/img.jpg')
# Resize = torchvision.transforms.Resize(416)
# img = Resize(img)

obstacle_detected = False
motor_values = []
forward_counter = 0

print("before detect")


def detect(img, model):
    global obstacle_detected, motor_values, forward_counter

    data_chunk = DataChunk()

    try:
        config = initiate_config()
        height_threshold = float(config.get('CONTROL', 'height_threshold'))
        base_speed = float(config.get('CONTROL', 'base_speed'))
        scaling_factor = float(config.get('CONTROL', 'scaling_factor'))
        confidence_threshold = float(config.get('CONTROL', 'confidence_threshold'))
        iou_threshold = float(config.get('CONTROL', 'iou_threshold'))
        forward_delay = int(config.get('CONTROL', 'forward_delay'))
    except configparser.Error as e:
        raise ConfigError(f"Error reading configuration: {e}")

    classes = ['obstacle']
    # with open('coco.names', 'r') as f:
    #     classes = [line.strip() for line in f.readlines()]

    # Load an image for detection
    #     img = Image.open('img.jpg')

    img = Image.fromarray(img)
    # Transforming the image to this size as this was the image size used in the training
    transform = transforms.Compose([
        transforms.Resize((416, 416)),
        transforms.ToTensor(),
        #     transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ])
    img_tensor = transform(img)

    # Perform object detection on the image
    results = model(img)

    boxes = results.xyxy[0].cpu().numpy()[:, :4]
    class_ids = results.pred[0].cpu().numpy()[:, 5].astype(int)
    confidences = results.pred[0].cpu().numpy()[:, 4]

    # non-max suppression to remove overlapping bounding boxes
    indices = torchvision.ops.nms(torch.from_numpy(boxes), torch.from_numpy(confidences), iou_threshold=0.5)

    # Draw bounding boxes on the image for objects with confidence score above the threshold
    colors = torch.tensor([[0, 255, 0], [0, 0, 255], [255, 0, 0]])
    if len(indices) > 0:
        obstacle_detected = True
        forward_counter = 0

        draw = ImageDraw.Draw(img)

        # TODO: If there is more than one obstacle, set the confidence value to a lower value, and then send a trigger.
        # or get the argmax confidence.
        for i in indices:
            x1, y1, x2, y2 = boxes[i]
            label = f'{classes[class_ids[i]]} {confidences[i]:.2f}'
            color = colors[class_ids[i]]
            draw.rectangle([x1, y1, x2, y2], width=2, outline=tuple(color))
            draw.text((x1, y1 - 10), label, fill=tuple(color))

            center_x = (x1 + x2) // 2
            center_y = (y1 + y2) // 2
            draw.ellipse([center_x - 2, center_y - 2, center_x + 2, center_y + 2], fill=tuple(color),
                         outline=tuple(color))

            im_width, im_height = img.size
            draw.ellipse([im_width // 2 - 2, im_height // 2 - 2, im_width // 2 + 2, im_height // 2 + 2],
                         fill=(255, 255, 255), outline=(255, 255, 255))
            error = (center_x - im_width / 2) / (im_width / 2)  # error is negative and normalized
            height = y2 - y1
            logging.debug("Height : " + str(height))
            logging.debug("Error : " + str(error))

            if height <= height_threshold:
                data_chunk.left_motor_speed = str(base_speed)
                data_chunk.right_motor_speed = str(base_speed)
                data_chunk.confidence = str(confidences[i])
                data_chunk.is_completed = False
            else:

                motor_value = base_speed + scaling_factor * (1 + error)

                motor_value = max(0, min(1, motor_value))

                data_chunk.left_motor_speed = str(motor_value)
                data_chunk.right_motor_speed = str(base_speed)
                data_chunk.confidence = str(confidences[i])
                data_chunk.is_completed = False

                motor_values.append((data_chunk.left_motor_speed, data_chunk.right_motor_speed))  # store motor values tuple (lm, rm)

    elif len(motor_values) > 0:
        forward_counter += 1

        if forward_counter <= forward_delay:
            # Move forward for the specified delay

            data_chunk.left_motor_speed = str(base_speed)
            data_chunk.right_motor_speed = str(base_speed)
            data_chunk.confidence = 1
            data_chunk.is_completed = False

        else:
            # Retrieve the last command from the stack and use it in reverse order
            left_motor, right_motor = motor_values.pop(0)
            # Setting motor values in reverse order.
            data_chunk.left_motor_speed = str(right_motor)
            data_chunk.right_motor_speed = str(left_motor)
            data_chunk.confidence = 1
            data_chunk.is_completed = False

            if len(motor_values) == 0:
                data_chunk.is_completed = True
    else:
        # If no obstacle is detected.
        # TODO: If there is no obstacle detected initially, set the confidence value to a lower value, and then send a trigger.
        data_chunk.is_completed = True

    return np.asarray(img), data_chunk

# out = detect(np.array(img), model)
