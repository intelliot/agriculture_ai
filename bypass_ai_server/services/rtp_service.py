import configparser
import logging

import requests


class APIError(Exception):
    pass


class ConfigError(Exception):
    pass


def initiate_config():
    config = configparser.ConfigParser()
    config.read('./config.ini')
    # config.read('../config.ini')
    return config


def get_rtp_handler():
    """
    returns rtp handler
    """
    config = initiate_config()

    try:
        base_url = config.get('API', 'base_url')
        camera = config.get('API', 'camera')
    except configparser.Error as e:
        raise ConfigError(f"Error reading configuration: {e}")

    endpoint = '/sensors/camera/handleRtpStream/'
    path_param = camera

    url = base_url + endpoint + path_param

    try:
        logging.info(f"Calling {url} to RTP handler")
        response = requests.get(url)

        if response.status_code == 200:

            data = response.json()
            return data
        else:
            # Error in the request
            raise APIError(f"Request failed with the following error: {response.json()}")
    except requests.RequestException as e:
        raise APIError(f"Error sending HTTP request: {e}")


def start_rtp_stream():
    config = initiate_config()

    try:
        base_url = config.get('API', 'base_url')
        camera = config.get('API', 'camera')
    except configparser.Error as e:
        raise ConfigError(f"Error reading configuration: {e}")

    endpoint = '/sensors/startRTPStream/'
    path_param = camera

    url = base_url + endpoint + path_param

    try:
        logging.info(f"Calling {url} to start the RPT stream")
        response = requests.put(url)

        if response.status_code == 200:
            data = response.json()
            logging.info(data)
            if data.get('was_success'):  ## TODO: Check whether this value is okay.
                return True
            else:
                return False
        else:
            # Error in the request
            raise APIError(f"Request failed with status code: {response}")
    except requests.RequestException as e:
        raise APIError(f"Error sending HTTP request: {e}")


def get_callback_url():
    config = initiate_config()

    try:
        callback_source_url = config.get('API', 'callback_url')
    except configparser.Error as e:
        raise ConfigError(f"Error reading configuration: {e}")

    url = callback_source_url

    try:
        response = requests.get(url)

        if response.status_code == 200:
            data = response.json()
            return data
        else:
            # Error in the request
            raise APIError(f"Request failed with status code: {response}")
    except requests.RequestException as e:
        raise APIError(f"Error sending HTTP request: {e}")


def notify_agent(callback_url):
    data = {'message': 'obstacle_detected'}

    try:
        response = requests.post(callback_url, data)

        if response.status_code == 200:
            logging.info("Successfully sent 'obstacle_detected' to the callback URL.")
        else:
            raise APIError(f"Failed to send data. Response status code: {response.status_code}")
    except requests.RequestException as e:
        raise APIError(f"Error sending HTTP request: {e}")
