import torch
import logging
import bypass_ai_server.logger as logger


class ModelManager:
    _model = None

    @classmethod
    def get_model(cls):
        if cls._model is None:
            logging.info("Loading the model...")
            cls._model = torch.hub.load('ultralytics/yolov5:v6.2', 'custom'
                                        , path='./weights/best.pt'
                                        , force_reload=False, verbose=False
                                        )

            # Remove external logging handlers.
            logger.remove_external_stream_handler()
            logging.info("Loading the model is completed")

        return cls._model
