import configparser
import logging

import requests

from bypass_ai_server.services.rtp_service import APIError, ConfigError


class AgentNotRegisteredError(Exception):
    pass


callback_urls = []


def initiate_config():
    config = configparser.ConfigParser()
    config.read('./config.ini')
    # config.read('../config.ini')
    return config

def register_callback_url(callback_url):
    if callback_url in callback_urls:
        return False  # URL is already registered
    callback_urls.append(callback_url)
    return True


def unregister_callback_url(callback_url):
    if callback_url in callback_urls:
        callback_urls.remove(callback_url)
        return True
    return False


def notify_agent(message="Obstacle-detected"):

    if len(callback_urls) == 0:
        try:
            config = initiate_config()
            callback_url = config.get('API', 'callback_source')
        except configparser.Error as e:
            raise ConfigError(f"Error reading configuration: {e}")
        # raise AgentNotRegisteredError(f"No agent has been registered")
    else:
        callback_url = callback_urls[-1]

    try:
        logging.info(f"Notifying the agent: {callback_url} with the message : {message}")
        response = requests.post(callback_url, json={"message": message})

        if response.status_code == 200:
            logging.info(f"Notification sent to {callback_url}")
        else:
            logging.error(f"Failed to send notification to {callback_url}: {response.status_code}")
    except Exception as e:
        logging.error(f"Failed to send notification to {callback_url}: {str(e)}")
        raise APIError(f"Failed to send notification")
