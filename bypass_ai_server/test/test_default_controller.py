# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from bypass_ai_server.models.compute_control_commands_response import ComputeControlCommandsResponse  # noqa: E501
from bypass_ai_server.models.data_chunk import DataChunk  # noqa: E501
from bypass_ai_server.models.error_response import ErrorResponse  # noqa: E501
from bypass_ai_server.models.get_stream_response import GetStreamResponse  # noqa: E501
from bypass_ai_server.models.validation_error import ValidationError  # noqa: E501
from bypass_ai_server.test import BaseTestCase


class TestDefaultController(BaseTestCase):
    """DefaultController integration test stubs"""

    def test_compute_control_commands_get(self):
        """Test case for compute_control_commands_get

        request AI to start computing control commands
        """
        response = self.client.open(
            '/compute_controlCommands',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_data_stream_get(self):
        """Test case for data_stream_get

        Retrieve data stream
        """
        response = self.client.open(
            '/data-stream',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_g_et_control_stream_get(self):
        """Test case for g_et_control_stream_get

        Returns handler corresponding to the stream of control commands
        """
        response = self.client.open(
            '/GET_controlStream',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_register_callback_url(self):
        """Test case for register_callback_url

        Register Callback URL of the Agent
        """
        data = dict(callback_url='callback_url_example')
        response = self.client.open(
            '/register_agent',
            method='POST',
            data=data,
            content_type='application/x-www-form-urlencoded')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
